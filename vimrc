set nocompatible
set hlsearch
set incsearch
set ignorecase
set noshowmode
set ruler
set number
set cursorline
set nobackup
set noswapfile
set backspace=2
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set expandtab
set showmatch
filetype indent on
set encoding=utf8
set fileencoding=utf8
set termencoding=utf8
set nobomb
set fileformat=unix
colors molokai
syntax enable
nmap <silent> <F5> :NERDTree<CR>
nmap <silent> <F3> :tabp <CR>
nmap <silent> <F4> :tabn <CR>
nnoremap <F2> :set nonumber!<CR>:set foldcolumn=0<CR>
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%79v.\+/
set pastetoggle=<F1>
set t_Co=256
source /Library/Python/2.7/site-packages/powerline/bindings/vim/plugin/powerline.vim
set laststatus=2

